# Overview

## [![Homepage](https://s3.amazonaws.com/assets.heroku.com/addons.heroku.com/icons/1347/original.png)](https://www.informantapp.com) informant-rails-example

## informant-rails

This project provides a simple example of integrating the
[informant-rails](https://gitlab.com/informantapp/informant-rails) gem with
a Ruby on Rails project.

## Setup

1. Clone this repo.
2. Create a new app on [the Informant](https://console.informantapp.com).
3. Copy `.env.example` to `.env`.
4. Copy the API key from the Informant web interface into your env file.
5. `bundle install`
6. Run `bundle exec rails s`.
7. Visit https://localhost:9292
