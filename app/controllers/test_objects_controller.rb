class TestObjectsController < ApplicationController
  def create
    if TestObject.new(test_object_attributes).valid?
      render :successful_validation
    else
      render :unsuccessful_validation
    end
  end

  protected

  def test_object_attributes
    params.require(:test_object).permit(:name)
  end
end
