Rails.application.routes.draw do
  resources :test_objects

  root to: 'test_objects#index'
end
