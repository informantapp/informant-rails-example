class CreateTestObjects < ActiveRecord::Migration[6.0]
  def change
    create_table :test_objects do |t|
      t.string :name
      t.timestamps
    end
  end
end
